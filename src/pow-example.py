from hashlib import sha256

x = 5
y = 0  # We don't know what y should be yet...

# 1. Getting a hash and looking on it's last digit.
# 2. Trying to add 1 to find correct hash
while sha256(f'{x*y}'.encode()).hexdigest()[-1] != '0':
    y += 1

print(f'The solution is y = {y}')

# While Produced hash end in 0:
# hash(5 * 21) = 1253e9373e...5e3600155e860 — it' True

# Bitcoin has the same algorithm called Hashcash
# Miners use this algorithm to solve the issue of the block
